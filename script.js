var canvas = document.getElementById("game");
var ctx = canvas.getContext("2d");

canvas.width = window.innerWidth;
canvas.height = window.innerHeight;


function drawRect(x,y,width,height,color){
    ctx.fillStyle = color;
    ctx.fillRect(x,y,width,height);
}

var gamezone = {
    width: canvas.width,
    height: canvas.height,
    background_color: "#000",
    

    initialize: function() {
        drawRect(10,10 ,this.width ,this.width);
    }

}

var player1 = {
    
    color: '#fff',

    width: 25,
    height: 100,
    posx: 90,
    posy: gamezone.height/2,
    speed: 1,

    draw: function(){
        drawRect(this.posx,this.posy,this.width,this.height,this.color);
    },

    move : function(e){
        if(e.key == "ArrowUp"){
        this.posy += this.speed;
        this.draw();}
        else if(e.key == "ArrowDown"){
        this.posy -= this.speed;
        this.draw();}
        return;
    },


}

var player2 = {

    color: '#fff',

    width: 25,
    height: 100,
    posx: canvas.width - 90,
    posy: gamezone.height/2,
    speed: 1,
    

    draw: function(){
        drawRect(this.posx,this.posy,this.width,this.height,this.color);
        return;
    },

    move : function(e){
        if(e.key == "w"){
        this.posy += this.speed;
        this.draw();}
        else if(e.key == "s"){
        this.posy -= this.speed;
        this.draw();
    }
        return;
    },

}

gamezone.initialize();
player1.draw();
player2.draw();

document.addEventListener('keydown',player1.move);
document.addEventListener('keydown',player2.move);

